(** Some dialog boxes for GUI *)

(** Open a window to chose a server and join an online game *)
val join_game : unit -> unit

(** Open a dialog to chose a map txt file, then ask for different options
 * whether the boolean is true (multiplayer map) or false (local game) *)
val new_game : bool -> unit
